// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { initializeApp } from "firebase/app";

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBbo4L08V_L-aYVHJ4KZfD2pAWM_1hdVvM",
    authDomain: "biblioteca-1fb25.firebaseapp.com",
    projectId: "biblioteca-1fb25",
    storageBucket: "biblioteca-1fb25.appspot.com",
    messagingSenderId: "410784521297",
    appId: "1:410784521297:web:90c96a37ac7f53e76fec70"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
