import {Socio} from "../interfaces/socio";

export const SOCIOS: Socio[] = [
  {
    id: '1',
    nombreCompleto: "Raúl López Rocha",
    nickname: "Rochaomg",
    prestamos: []
  },
  {
    id: '2',
    nombreCompleto: "Carlos Gutiérrez Mairena",
    nickname: "Cakire",
    prestamos: []
  },
  {
    id: '3',
    nombreCompleto: "Elena García Herrera",
    nickname: "xXEliNa1Xx",
    prestamos: []
  },
  {
    id: '4',
    nombreCompleto: "Pedro Heredia Martinez",
    nickname: "Pedredude",
    prestamos: []
  },
  {
    id: '5',
    nombreCompleto: "Ana Carozo Román",
    nickname: "MakiAne",
    prestamos: []
  },
  {
    id: '6',
    nombreCompleto: "Alberto De la Vega López",
    nickname: "Juti",
    prestamos: []
  }
]


