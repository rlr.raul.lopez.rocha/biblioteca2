import {Libro} from "../interfaces/libro";

export const LIBROS: Libro[] = [
  {
    id: '1',
    titulo: "El Mío Cid",
    autor: "Abu l-Walid al Waqqashi",
    genero: "Epopeya",
    fechaCreacion: "??/??/1307",
    prestado: false
  },
  {
    id: '2',
    titulo: "Crepúsculo",
    autor: "Stephenie Meyer",
    genero: "Fantástico, Romance, Drama, Terror",
    fechaCreacion: "05/10/2005",
    prestado: true
  },
  {
    id: '3',
    titulo: "Nacidos de la Bruma",
    autor: "Brandon Sanderson",
    genero: "Alta Fantasía",
    fechaCreacion: "??/??/2006",
    prestado: false
  },
  {
    id: '4',
    titulo: "El asesinato de Pitágoras",
    autor: "Marcos Chicot",
    genero: "Histórico, Ficción, Suspense",
    fechaCreacion: "??/??/2013",
    prestado: false
  }
]
