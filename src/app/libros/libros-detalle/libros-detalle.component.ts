import { Component, OnInit } from '@angular/core';
import {Libro} from "../../interfaces/libro";
import {ActivatedRoute, ParamMap, Route} from "@angular/router";
import {FirestoreService} from "../../services/firestore.service";
import {Observable} from "rxjs";
import firebase from "firebase/compat";
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;
import {Action, AngularFirestore} from "@angular/fire/compat/firestore";
import {Socio} from "../../interfaces/socio";
import {Prestamo} from "../../interfaces/prestamo";
import {ConexionService} from "../../services/conexion.service";

@Component({
  selector: 'app-libros-detalle',
  templateUrl: './libros-detalle.component.html',
  styleUrls: ['./libros-detalle.component.css']
})
export class LibrosDetalleComponent implements OnInit {

  editarLibro: Libro = {
    titulo: '',
    id: '',
    autor: '',
    genero: '',
    fechaCreacion: '',
    prestado: false
  }
  libro: Libro = {
    id: '',
    titulo: '',
    autor: '',
    genero: '',
    fechaCreacion: '',
    prestado: false
  }
  buscar?: string;
  libros: any;
  socios: Socio[] = [];
  prestamos: Prestamo[] = [];
  prestamo: Prestamo = {
    id: '',
    libroId: '',
    socioId: '',
    fechaInicio: new Date(),
    fechaRegreso: new Date()
  }
  idLibro: any;

  constructor(private route: ActivatedRoute, private firestore: FirestoreService, private conexion: ConexionService) {
    this.conexion.listaLibro().subscribe(librosAfs => {
      this.libros = librosAfs;
    });
    this.conexion.listaPrestamo().subscribe(prestamoAfs => {
      this.prestamos = prestamoAfs;
    })
  }

  ngOnInit(): void {
    this.prestamo = {id: '', socioId: '', libroId: '', fechaInicio: new Date(), fechaRegreso: new Date()};
    this.route.paramMap
      .subscribe((params: ParamMap) => { // SUBSCRIBE -> Estás pendiente de que la ruta "cambie"
        let id = <string>params.get('id');
        this.firestore.getLibro(id).subscribe((librosm: any) => {
          this.libro = librosm.payload.data();
          this.idLibro = librosm.payload.id;
        })
        });
    this.firestore.getSocios().subscribe((sociosSnapshot) => {
      this.socios = [];
      sociosSnapshot.forEach((socioData: any) => {
        this.socios.push({
          nombreCompleto: socioData.payload.doc.data().nombreCompleto,
          nickname: socioData.payload.doc.data().nickname,
          prestamos: socioData.payload.doc.data().prestamos,
          id: socioData.payload.doc.id,
        });
      })
    });
  }

  editar(libro: Libro) {
    this.editarLibro = libro;
  }

  prestar(idSocio: string) {
    let ahora: Date = new Date();
    let prestamo;
    //this.prestamo.libroId = <number>this.libro?.id;
    // @ts-ignore
    this.prestamo.socioId = idSocio;
    this.prestamo.id = (this.prestamos.length + 1).toString();
    // @ts-ignore
    this.prestamo.fechaInicio = ahora;
    // @ts-ignore
    this.prestamo.fechaRegreso = ahora.getDate() + 7;
    // @ts-ignore
    this.prestamo.libroId = this.idLibro;
    this.libro.prestado = true;
    this.libro.id = this.idLibro;
    this.conexion.editarLibro(this.libro, this.libro.id.toString());
  }

    confirmar(socio: Socio) {
    let text = "¿Prestar este libro al usuario " + socio.nickname + "?" ;
    if (confirm(text) == true) {
      this.prestar(socio.id)
      // @ts-ignore
      socio.prestamos.push(this.prestamo);
      this.conexion.agregarPrestamo(this.prestamo);
      // @ts-ignore
      this.firestore.updateSocio(socio.id, socio);
    }
  }

  eliminar(libro: Libro, idLibro: any) {
    if(confirm("¿Estás seguro que deseas eliminar el libro " + libro.titulo + "?"))
      this.conexion.eliminarLibro(libro, idLibro);
  }

  agregarEdit(idLibro: any) {
    console.log(idLibro);
    this.conexion.editarLibro(this.editarLibro, idLibro)
  }
}
