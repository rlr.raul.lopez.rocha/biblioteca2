import { Component, OnInit } from '@angular/core';
import {Libro} from "../interfaces/libro";
import {LIBROS} from "../mocks/libros-mock";
import {FirestoreService} from "../services/firestore.service";
import {Observable} from "rxjs";
import {Route, Router} from "@angular/router";
import {filter} from "rxjs/operators";
import {DocumentChangeAction} from "@angular/fire/compat/firestore";
import {AngularFireList} from "@angular/fire/compat/database";
import {ConexionService} from "../services/conexion.service";

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  libros: Libro[] = [];

  constructor(private firestore: FirestoreService, private router: Router, private conexion: ConexionService) {
    this.conexion.listaLibro().subscribe((librosAfs) => {
      this.libros = librosAfs;
    })
  }

  ngOnInit(): void {
    this.firestore.getLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          autor: libroData.payload.doc.data().autor,
          fechaCreacion: libroData.payload.doc.data().fechaCreacion,
          genero: libroData.payload.doc.data().genero,
          prestado: libroData.payload.doc.data().prestado,
          titulo: libroData.payload.doc.data().titulo,
          id: libroData.payload.doc.id,
        });
      })
    });
  }

}
