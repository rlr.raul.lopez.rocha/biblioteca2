import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibrosRoutingModule } from './libros-routing.module';
import { LibrosDetalleComponent } from './libros-detalle/libros-detalle.component';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatOptionModule} from "@angular/material/core";
import { LibrosCreacionComponent } from './libros-creacion/libros-creacion.component';
import {FormsModule} from "@angular/forms";
import { LibroEdicionComponent } from './libro-edicion/libro-edicion.component';


@NgModule({
  declarations: [
    LibrosDetalleComponent,
    LibrosCreacionComponent,
    LibroEdicionComponent
  ],
    imports: [
        CommonModule,
        LibrosRoutingModule,
        MatButtonToggleModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        FormsModule
    ]
})
export class LibrosModule { }
