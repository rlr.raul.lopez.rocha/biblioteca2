import { Component, OnInit } from '@angular/core';
import {Libro} from "../../interfaces/libro";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {FirestoreService} from "../../services/firestore.service";
import {ConexionService} from "../../services/conexion.service";

@Component({
  selector: 'app-libro-edicion',
  templateUrl: './libro-edicion.component.html',
  styleUrls: ['./libro-edicion.component.css']
})
export class LibroEdicionComponent implements OnInit {

  editarLibro: Libro = {
    titulo: '',
    id: '',
    autor: '',
    genero: '',
    fechaCreacion: '',
    prestado: false
  }
  libros: Libro[] = [];
  libro?: Libro;
  idLibro: any;

  constructor(private route: ActivatedRoute, private firestore: FirestoreService, private conexion: ConexionService) {
    this.conexion.listaLibro().subscribe(librosAfs => {
      this.libros = librosAfs;
    })
  }

  ngOnInit(): void {
    this.route.paramMap
      .subscribe((params: ParamMap) => { // SUBSCRIBE -> Estás pendiente de que la ruta "cambie"
        let id = <string>params.get('id');
        this.firestore.getLibro(id).subscribe((librosm: any) => {
          this.libro = librosm.payload.data();
          this.idLibro = librosm.payload.id;
          console.log(this.libro);
        })
      });
  }

  libroEdit(idLibro: any) {
    console.log(idLibro);
    this.conexion.editarLibro(this.editarLibro, idLibro)
  }

}
