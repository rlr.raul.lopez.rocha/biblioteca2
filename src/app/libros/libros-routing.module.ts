import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LibrosComponent} from "./libros.component";
import {LibrosDetalleComponent} from "./libros-detalle/libros-detalle.component";
import {LibrosCreacionComponent} from "./libros-creacion/libros-creacion.component";
import {LibroEdicionComponent} from "./libro-edicion/libro-edicion.component";

const routes: Routes = [
  { path: '', component: LibrosComponent },
  { path: 'libro/:id', component: LibrosDetalleComponent },
  { path: 'creaLibro', component: LibrosCreacionComponent},
  { path: 'libro/:id/editLibro', component: LibroEdicionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
