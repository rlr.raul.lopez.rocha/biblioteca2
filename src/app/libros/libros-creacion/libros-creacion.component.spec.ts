import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibrosCreacionComponent } from './libros-creacion.component';

describe('LibrosCreacionComponent', () => {
  let component: LibrosCreacionComponent;
  let fixture: ComponentFixture<LibrosCreacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibrosCreacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibrosCreacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
