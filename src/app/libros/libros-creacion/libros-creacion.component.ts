import { Component, OnInit } from '@angular/core';
import {Libro} from "../../interfaces/libro";
import {ConexionService} from "../../services/conexion.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-libros-creacion',
  templateUrl: './libros-creacion.component.html',
  styleUrls: ['./libros-creacion.component.css']
})
export class LibrosCreacionComponent implements OnInit {

  libros: Observable<Libro[]>;
  libro: Libro = {
    id: '',
    titulo: '',
    autor: '',
    fechaCreacion: '',
    genero: '',
    prestado: false
  }

  constructor(private conexion: ConexionService) {
    this.libros = conexion.listaLibro();
  }

  ngOnInit(): void {
  }

  agregar() {
    /*this.libros.subscribe(librosm => {
      this.libro.id = librosm.length + 1;
    })*/
    this.conexion.agregarLibro(this.libro);
    this.libro.titulo = '';
    this.libro.autor = '';
    this.libro.genero = '';
    this.libro.fechaCreacion = '';
    this.libro.id = '';
  }
}
