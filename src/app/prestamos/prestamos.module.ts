import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestamosRoutingModule } from './prestamos-routing.module';
import { PrestamosDetalleComponent } from './prestamos-detalle/prestamos-detalle.component';


@NgModule({
  declarations: [
    PrestamosDetalleComponent
  ],
  imports: [
    CommonModule,
    PrestamosRoutingModule
  ]
})
export class PrestamosModule { }
