import { Component, OnInit } from '@angular/core';
import {Socio} from "../../interfaces/socio";
import {Libro} from "../../interfaces/libro";
import {Prestamo} from "../../interfaces/prestamo";
import {FirestoreService} from "../../services/firestore.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {ConexionService} from "../../services/conexion.service";

@Component({
  selector: 'app-prestamos-detalle',
  templateUrl: './prestamos-detalle.component.html',
  styleUrls: ['./prestamos-detalle.component.css']
})
export class PrestamosDetalleComponent implements OnInit {

  prestamo: Prestamo = {
    id: '',
    libroId: '',
    socioId: '',
    fechaInicio: new Date(),
    fechaRegreso: new Date()
  }
  idPrestamo: string = '';
  socios: Socio[] = [];
  libro: Libro = {
    id: '',
    titulo: '',
    autor: '',
    genero: '',
    fechaCreacion: '',
    prestado: false
  }
  socio: Socio = {
    id: '',
    nombreCompleto: '',
    nickname: '',
    prestamos: []
  }
  libros: Libro[] = [];
  prestamos: Prestamo[] = [];

  constructor(private firestore: FirestoreService, private route: ActivatedRoute, private conexion: ConexionService) {
    this.conexion.listaPrestamo().subscribe((prestamosAfs) => {
      this.prestamos = prestamosAfs;
    });
    this.conexion.listaSocio().subscribe((sociosAfs) => {
      this.socios = sociosAfs;
    });
  }

  ngOnInit(): void {
    this.firestore.getLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          autor: libroData.payload.doc.data().autor,
          fechaCreacion: libroData.payload.doc.data().fechaCreacion,
          genero: libroData.payload.doc.data().genero,
          prestado: libroData.payload.doc.data().prestado,
          titulo: libroData.payload.doc.data().titulo,
          id: libroData.payload.doc.id,
        });
      })
    });
    console.log(this.libros);
    this.route.paramMap
      .subscribe((params: ParamMap) => { // SUBSCRIBE -> Estás pendiente de que la ruta "cambie"
        let id = <string>params.get('id');
        this.firestore.getPrestamo(id).subscribe((prestamosm: any) => {
          this.prestamo = prestamosm.payload.data();
          this.idPrestamo = prestamosm.payload.id;
        })
      });
    for(let libro of this.libros) {
      if(libro.id === this.prestamo.libroId)
        this.libro = libro;
    }
    for(let socio of this.socios) {
      if(socio.id == this.prestamo.socioId)
        this.socio = socio;
    }
    console.log(this.socio);
    console.log(this.libro);
  }

}
