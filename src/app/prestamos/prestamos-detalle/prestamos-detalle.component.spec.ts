import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestamosDetalleComponent } from './prestamos-detalle.component';

describe('PrestamosDetalleComponent', () => {
  let component: PrestamosDetalleComponent;
  let fixture: ComponentFixture<PrestamosDetalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrestamosDetalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestamosDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
