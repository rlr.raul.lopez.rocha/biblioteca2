import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PrestamosComponent} from "./prestamos.component";
import {PrestamosDetalleComponent} from "./prestamos-detalle/prestamos-detalle.component";

const routes: Routes = [
  { path: '', component: PrestamosComponent },
  { path: 'prestamo/:id', component: PrestamosDetalleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestamosRoutingModule { }
