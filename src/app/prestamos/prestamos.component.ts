import { Component, OnInit } from '@angular/core';
import {FirestoreService} from "../services/firestore.service";
import {Router} from "@angular/router";
import {ConexionService} from "../services/conexion.service";
import {Prestamo} from "../interfaces/prestamo";
import {Observable} from "rxjs";
import {Libro} from "../interfaces/libro";
import {Socio} from "../interfaces/socio";

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.css']
})
export class PrestamosComponent implements OnInit {

  prestamo: Prestamo = {
    id: '',
    libroId: '',
    socioId: '',
    fechaInicio: new Date(),
    fechaRegreso: new Date()
  }
  socios: Socio[] = [];
  libros: Libro[] = [];
  prestamos: Prestamo[] = [];

  constructor(private firestore: FirestoreService, private router: Router, private conexion: ConexionService) {
    this.conexion.listaPrestamo().subscribe((prestamosAfs) => {
      this.prestamos = prestamosAfs;
    });
    this.conexion.listaLibro().subscribe((librosAfs) => {
      this.libros = librosAfs;
    });
    this.conexion.listaSocio().subscribe((sociosAfs) => {
      this.socios = sociosAfs;
    });
  }

  ngOnInit(): void {
    this.firestore.getPrestamos().subscribe((prestamosSnapshot) => {
      this.prestamos = [];
      prestamosSnapshot.forEach((prestamoData: any) => {
        this.prestamos.push({
          libroId: prestamoData.payload.doc.data().libroId,
          socioId: prestamoData.payload.doc.data().socioId,
          fechaInicio: prestamoData.payload.doc.data().fechaInicio,
          fechaRegreso: prestamoData.payload.doc.data().fechaRegreso,
          id: prestamoData.payload.doc.id,
        });
      })
    });
    console.log(this.prestamos);
  }

  devolver(prestamoMain: Prestamo) {
    for(let libro of this.libros) {
      if(libro.id == prestamoMain.libroId) {
        libro.prestado = false;
        this.conexion.editarLibro(libro, libro.id);
      }
    }
    
    for(let socio of this.socios) {
      if(socio.id == prestamoMain.socioId) {
        for(let prestamo of socio.prestamos) {
          if(prestamo.id == prestamoMain.id) {
            socio.prestamos.splice(socio.prestamos.indexOf(prestamo)-1,1);
            console.log(socio.prestamos);
            this.conexion.editarSocio(socio, socio.id);
          }
        }
      }
    }
    this.conexion.eliminarPrestamo(prestamoMain, prestamoMain.id);
  }
}
