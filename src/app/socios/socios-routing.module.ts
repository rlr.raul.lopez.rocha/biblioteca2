import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SociosComponent} from "./socios.component";
import {SociosDetalleComponent} from "./socios-detalle/socios-detalle.component";
import {SociosCreacionComponent} from "./socios-creacion/socios-creacion.component";
import {SocioEdicionComponent} from "./socio-edicion/socio-edicion.component";

const routes: Routes = [
  { path: '', component: SociosComponent },
  { path: 'socio/:id', component: SociosDetalleComponent },
  { path: 'creaSocio', component: SociosCreacionComponent },
  { path: 'socio/:id/editaSocio', component: SocioEdicionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SociosRoutingModule { }
