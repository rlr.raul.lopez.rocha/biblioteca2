import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SociosCreacionComponent } from './socios-creacion.component';

describe('SociosCreacionComponent', () => {
  let component: SociosCreacionComponent;
  let fixture: ComponentFixture<SociosCreacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SociosCreacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SociosCreacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
