import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Libro} from "../../interfaces/libro";
import {ConexionService} from "../../services/conexion.service";
import {Socio} from "../../interfaces/socio";

@Component({
  selector: 'app-socios-creacion',
  templateUrl: './socios-creacion.component.html',
  styleUrls: ['./socios-creacion.component.css']
})
export class SociosCreacionComponent implements OnInit {

  socios: Observable<Socio[]>;
  socio: Socio = {
    id: '',
    nombreCompleto: '',
    nickname: '',
    prestamos: []
  }

  constructor(private conexion: ConexionService) {
    this.socios = conexion.listaSocio();
  }

  ngOnInit(): void {
  }

  agregar() {
    /*this.socios.subscribe(sociosm => {
      this.socio.id = sociosm.length + 1;
    })*/
    this.conexion.agregarSocio(this.socio);
    this.socio.nickname = '';
    this.socio.nombreCompleto = '';
    this.socio.prestamos = [];
    this.socio.id = '';
  }

}
