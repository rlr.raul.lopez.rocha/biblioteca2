import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SociosRoutingModule } from './socios-routing.module';
import { SociosDetalleComponent } from './socios-detalle/socios-detalle.component';
import { SociosCreacionComponent } from './socios-creacion/socios-creacion.component';
import {FormsModule} from "@angular/forms";
import { SocioEdicionComponent } from './socio-edicion/socio-edicion.component';


@NgModule({
  declarations: [
    SociosDetalleComponent,
    SociosCreacionComponent,
    SocioEdicionComponent
  ],
    imports: [
        CommonModule,
        SociosRoutingModule,
        FormsModule
    ]
})
export class SociosModule { }
