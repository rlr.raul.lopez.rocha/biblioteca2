import { Component, OnInit } from '@angular/core';
import {Libro} from "../../interfaces/libro";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {FirestoreService} from "../../services/firestore.service";
import {Socio} from "../../interfaces/socio";

@Component({
  selector: 'app-socios-detalle',
  templateUrl: './socios-detalle.component.html',
  styleUrls: ['./socios-detalle.component.css']
})
export class SociosDetalleComponent implements OnInit {

  libros: Libro[] = [];
  socio?: Socio;
  buscar?: string;
  socios: Socio[] = [];

  constructor(private route: ActivatedRoute, private firestore: FirestoreService) { }

  ngOnInit(): void {
    this.firestore.getLibros().subscribe((librosSnapshot) => {
      this.libros = [];
      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          autor: libroData.payload.doc.data().autor,
          fechaCreacion: libroData.payload.doc.data().fechaCreacion,
          genero: libroData.payload.doc.data().genero,
          prestado: libroData.payload.doc.data().prestado,
          titulo: libroData.payload.doc.data().titulo,
          id: libroData.payload.doc.id,
        });
      })
    });
    this.route.paramMap
      .subscribe((params: ParamMap) => { // SUBSCRIBE -> Estás pendiente de que la ruta "cambie"
        let id = <string>params.get('id');
        this.firestore.getSocio(id).subscribe((sociosm: any) => {

          this.socio = sociosm.payload.data();
          console.log(this.socio);
        })
      });
  }
}
