import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SociosDetalleComponent } from './socios-detalle.component';

describe('SociosDetalleComponent', () => {
  let component: SociosDetalleComponent;
  let fixture: ComponentFixture<SociosDetalleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SociosDetalleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SociosDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
