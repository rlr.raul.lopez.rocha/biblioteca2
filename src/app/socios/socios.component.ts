import { Component, OnInit } from '@angular/core';
import {Libro} from "../interfaces/libro";
import {FirestoreService} from "../services/firestore.service";
import {Router} from "@angular/router";
import {Socio} from "../interfaces/socio";
import {ConexionService} from "../services/conexion.service";

@Component({
  selector: 'app-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.css']
})
export class SociosComponent implements OnInit {

  socios: Socio[] = [];
  idSocio: any;

  constructor(private firestore: FirestoreService, private router: Router, private conexion: ConexionService) {

  }

  ngOnInit(): void {
    this.firestore.getSocios().subscribe((sociosSnapshot) => {
      this.socios = [];
      sociosSnapshot.forEach((socioData: any) => {
        this.socios.push({
          nombreCompleto: socioData.payload.doc.data().nombreCompleto,
          nickname: socioData.payload.doc.data().nickname,
          prestamos: socioData.payload.doc.data().prestamos,
          id: socioData.payload.doc.id,
        });
      })
    });
  }

  eliminar(socio: Socio, idSocio: any) {
    this.conexion.eliminarSocio(socio, idSocio);
  }
}
