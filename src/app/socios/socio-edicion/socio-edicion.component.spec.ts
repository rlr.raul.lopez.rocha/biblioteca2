import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioEdicionComponent } from './socio-edicion.component';

describe('SocioEdicionComponent', () => {
  let component: SocioEdicionComponent;
  let fixture: ComponentFixture<SocioEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SocioEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
