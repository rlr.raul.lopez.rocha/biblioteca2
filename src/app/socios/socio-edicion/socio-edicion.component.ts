import { Component, OnInit } from '@angular/core';
import {Libro} from "../../interfaces/libro";
import {Socio} from "../../interfaces/socio";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {FirestoreService} from "../../services/firestore.service";
import {ConexionService} from "../../services/conexion.service";

@Component({
  selector: 'app-socio-edicion',
  templateUrl: './socio-edicion.component.html',
  styleUrls: ['./socio-edicion.component.css']
})
export class SocioEdicionComponent implements OnInit {

  editarSocio: Socio = {
    nombreCompleto: '',
    id: '',
    nickname: '',
    prestamos: []
  }
  socios: Socio[] = [];
  socio?: Socio;
  idSocio: any;

  constructor(private route: ActivatedRoute, private firestore: FirestoreService, private conexion: ConexionService) {
    this.conexion.listaSocio().subscribe(librosAfs => {
      this.socios = librosAfs;
    })
  }

  ngOnInit(): void {
    this.route.paramMap
      .subscribe((params: ParamMap) => { // SUBSCRIBE -> Estás pendiente de que la ruta "cambie"
        let id = <string>params.get('id');
        this.firestore.getSocio(id).subscribe((sociosm: any) => {
          this.socio = sociosm.payload.data();
          this.idSocio = sociosm.payload.id;
        })
      });
  }

  socioEdit(idSocio: any) {
    console.log(idSocio);
    this.conexion.editarSocio(this.editarSocio, idSocio)
  }
}
