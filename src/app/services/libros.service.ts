import { Injectable } from '@angular/core';
import {Libro} from "../interfaces/libro";
import {LIBROS} from "../mocks/libros-mock";

@Injectable({
  providedIn: 'root'
})
export class LibrosService {

  constructor() { }

  getLibros(): Libro[] {
    return LIBROS;
  }

  getLibro(id: number): Libro | null {
    for(let libro of LIBROS) {
      if(libro.id == id)
        return libro;
    }
    return null;
  }
}
