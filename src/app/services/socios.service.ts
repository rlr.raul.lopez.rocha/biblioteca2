import { Injectable } from '@angular/core';
import {Socio} from "../interfaces/socio";
import {SOCIOS} from "../mocks/socios-mock";

@Injectable({
  providedIn: 'root'
})
export class SociosService {

  constructor() { }

  getSocios(): Socio[] {
    return SOCIOS;
  }

  getSocio(id: number): Socio | null {
    for(let socio of SOCIOS) {
      if(socio.id == id)
        return socio;
    }
    return null;
  }

}
