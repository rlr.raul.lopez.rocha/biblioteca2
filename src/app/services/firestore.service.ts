import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Libro} from "../interfaces/libro";

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(
    private firestore: AngularFirestore
  ) {}

  //Crea un nuevo socio
  public createSocio(data: {nombreCompleto: string, nickname: string, prestamos: []}) {
    return this.firestore.collection('socios').add(data);
  }
  //Obtiene un socio
  public getSocio(documentId: string) {
    return this.firestore.collection('socios').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los socios
  public getSocios() {
    return this.firestore.collection('socios').snapshotChanges();
  }
  //Actualiza un socio
  public updateSocio(documentId: string, data: any) {
    return this.firestore.collection('socios').doc(documentId).set(data);
  }
  //Elimina un socio
  public deleteSocio(documentId: string) {
    return this.firestore.collection('socios').doc(documentId).delete();
  }

  // ----------------

  //Crea un nuevo libro
  public createLibro(data: {titulo: string, autor: string, genero: string, fechaCreacion: string, prestado: boolean}) {
    return this.firestore.collection('libros').add(data);
  }
  //Obtiene un libro
  public getLibro(documentId: string) {
    return this.firestore.collection('libros').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los libros
  public getLibros() {
    return this.firestore.collection('libros').snapshotChanges();
  }
  //Actualiza un libro
  public updateLibro(documentId: string, data: any) {
    return this.firestore.collection('libros').doc(documentId).set(data);
  }
  //Elimina un libro
  public deleteLibro(documentId: string) {
    return this.firestore.collection('libros').doc(documentId).delete();
  }

  // -----------------------------

  //Crea un nuevo prestamo
  public createPrestamo(data: {socioId: number, libroId: number, fechaInicio: Date, fechaRegreso: Date}) {
    return this.firestore.collection('prestamos').add(data);
  }
  //Obtiene un prestamo
  public getPrestamo(documentId: string) {
    return this.firestore.collection('prestamos').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los prestamos
  public getPrestamos() {
    return this.firestore.collection('prestamos').snapshotChanges();
  }
  //Actualiza un prestamo
  public updatePrestamo(documentId: string, data: any) {
    return this.firestore.collection('prestamos').doc(documentId).set(data);
  }
  //Elimina un prestamo
  public deletePrestamo(documentId: string) {
    return this.firestore.collection('prestamos').doc(documentId).delete();
  }
}
