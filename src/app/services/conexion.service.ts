import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from "@angular/fire/compat/firestore";
import {Observable} from 'rxjs';
import {Libro} from "../interfaces/libro";
import {Socio} from "../interfaces/socio";
import {Prestamo} from "../interfaces/prestamo";
import {FirestoreService} from "./firestore.service";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConexionService {

  private librosCollection: AngularFirestoreCollection<Libro>;
  libros: Observable<Libro[]>;
  // @ts-ignore
  private libroDoc: AngularFirestoreDocument<Libro>;
  private sociosCollection: AngularFirestoreCollection<Socio>;
  socios: Observable<Socio[]>;
  // @ts-ignore
  private socioDoc: AngularFirestoreDocument<Socio>;
  private prestamosCollection: AngularFirestoreCollection<Prestamo>;
  prestamos: Observable<Prestamo[]>;
  // @ts-ignore
  private prestamoDoc: AngularFirestoreDocument<Prestamo>;

  constructor(private afs: AngularFirestore, private firestore: FirestoreService) {
    this.librosCollection = afs.collection<Libro>('libros');
    this.libros = this.librosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Libro;
          const id = a.payload.doc.id;
          // @ts-ignore
          return { id, ...data };
        })
      })
    );
    this.sociosCollection = afs.collection<Socio>('socios');
    this.socios = this.sociosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Socio;
          const id = a.payload.doc.id;
          // @ts-ignore
          return { id, ...data };
        })
      })
    );
    this.prestamosCollection = afs.collection<Prestamo>('prestamos');
    this.prestamos = this.prestamosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Prestamo;
          const id = a.payload.doc.id;
          // @ts-ignore
          return { id, ...data };
        })
      })
    );
  }

  // LISTAS
  listaLibro() {
    return this.libros;
  }
  listaSocio() {
    return this.socios;
  }
  listaPrestamo() {
    return this.prestamos;
  }

  // AGREGAR
  agregarLibro(libro: Libro) {
    this.librosCollection.add(libro);
  }
  agregarSocio(socio: Socio) {
    this.sociosCollection.add(socio);
  }
  agregarPrestamo(prestamo: Prestamo) {
    this.prestamosCollection.add(prestamo);
  }

  // ELIMINAR
  eliminarLibro(libro: Libro, idLibro: string) {
    this.libroDoc = this.afs.doc<Libro>(`libros/${idLibro}`);
    this.libroDoc.delete();
  }
  eliminarSocio(socio: Socio, idSocio: string) {
    this.socioDoc = this.afs.doc<Socio>(`socios/${idSocio}`);
    this.socioDoc.delete();
  }
  eliminarPrestamo(prestamo: Prestamo, idPrestamo: string) {
    this.prestamoDoc = this.afs.doc<Prestamo>(`prestamos/${idPrestamo}`);
    this.prestamoDoc.delete();
  }

  // EDITAR
  editarLibro(libro: Libro, idLibro: string) {
    console.log(libro);
    this.libroDoc = this.afs.doc<Libro>(`libros/${idLibro}`);
    this.libroDoc.update(libro);
  }
  editarSocio(socio: Socio, idSocio: string) {
    this.socioDoc = this.afs.doc<Socio>(`socios/${idSocio}`);
    this.socioDoc.update(socio);
  }
  editarPrestamo(prestamo: Prestamo, idPrestamo: string) {
    this.prestamoDoc = this.afs.doc<Prestamo>(`prestamos/${idPrestamo}`);
    this.prestamoDoc.update(prestamo);
  }

}
