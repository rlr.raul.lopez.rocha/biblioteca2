import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LibrosModule} from "./libros/libros.module";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'socios', loadChildren: () => import('./socios/socios.module').then(m => m.SociosModule) },
  { path: 'libros', loadChildren: () => import('./libros/libros.module').then(m => m.LibrosModule) },
  { path: 'prestamos', loadChildren: () => import('./prestamos/prestamos.module').then(m => m.PrestamosModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
