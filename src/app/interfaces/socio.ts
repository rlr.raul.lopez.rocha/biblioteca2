import {Prestamo} from "./prestamo";

export interface Socio {
  id: string,
  nombreCompleto: string,
  nickname: string,
  prestamos: Prestamo[]
}
