export interface Prestamo {
  id: string,
  libroId: string,
  socioId: string,
  fechaInicio: Date,
  fechaRegreso: Date
}
