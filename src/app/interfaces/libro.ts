export interface Libro {
  id: string,
  titulo: string,
  autor: string,
  genero: string,
  fechaCreacion: string,
  prestado: boolean
}
